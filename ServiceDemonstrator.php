<?php
	class ServiceDemonstrator {
		private static $localizationArr = array();
		private static $localizationErr = array();
		private $text = '';
		private $selectedCheckboxes = array();
		private $selectedRadiobutton = '';
		private $selectedOption = '';
		private $result = '';
		private $resultArr = array();
		const BR = "<br>\n";

		function __construct($selectedCheckboxes = array(), $selectedRadiobutton = '', $selectedOption = '') {
			$this->selectedCheckboxes = $selectedCheckboxes;
			$this->selectedRadiobutton = $selectedRadiobutton;
			$this->selectedOption = $selectedOption;
		}

		public function setText($text) {
			$this->text = $text;
		}

		public static function loadLanguages() {
			$languages = array();
			$files = scandir('lang/');
			if(!empty($files)) {
				foreach($files as $file) {
					if(substr($file, 2, 4) == '.txt') {
						$languages[] = substr($file, 0, 2);
					}
				}
			}
			return $languages;
		}
		
		public static function loadLocalization($lang) {
			$filepath = "lang/$lang.txt";
			$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			if($linesArr === false) {
				$filepath = "lang/en.txt";
				$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			}
			foreach($linesArr as $line) {
				if(empty($line)) {
					$key = $value = '';
				}
				elseif(substr($line, 0, 1) !== '#' && substr($line, 0, 2) !== '//') {
					if(empty($key)) {
						$key = $line;
					}
					else {
						if(!isset(self::$localizationArr[$key])) {
							self::$localizationArr[$key] = $line;
						}
					}
				}
			}
		}
		
		public static function showMessage($msg) {
			if(isset(self::$localizationArr[$msg])) {
				return self::$localizationArr[$msg];
			}
			else {
				self::$localizationErr[] = $msg;
				return $msg;
			}
		}
		
		public static function sendErrorList($lang) {
			if(!empty(self::$localizationErr)) {
				$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
				$sendersName = 'Service Demonstrator';
				$recipient = 'corpus.by@gmail.com';
				$subject = "ERROR: Incorrect localization in $sendersName from IP $ip";
				$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
				$mailBody .= $_SERVER['HTTP_HOST'] . '/ServiceDemonstrator/ дасылае інфармацыю аб наступных памылках:' . self::BR . self::BR;
				$mailBody .= "Мова лакалізацыі: <b>$lang</b>" . self::BR;
				$mailBody .= 'Адсутнічае лакалізацыя наступных ідэнтыфікатараў:' . self::BR . implode(self::BR, self::$localizationErr) . self::BR;
				$header  = "MIME-Version: 1.0\r\n";
				$header .= "Content-type: text/html; charset=utf-8\r\n";
				$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
				mail($recipient, $subject, $mailBody, $header);
			}
		}
		
		public function run() {
			$this->result .= self::showMessage('entered text') . "\n" . $this->text . "\n";
			$this->result .= self::showMessage('selected settings') . "\n";
			if(!empty($this->selectedCheckboxes['checkbox1'])) {
				$this->result .= self::showMessage('user select') . ' ' . self::showMessage('checkbox1') . "\n";
				$this->resultArr[self::showMessage('checkbox1')] = self::showMessage('user select') . ' ' . self::showMessage('checkbox1');
			}
			if(!empty($this->selectedCheckboxes['checkbox2'])) {
				$this->result .= self::showMessage('user select') . ' ' . self::showMessage('checkbox2') . "\n";
				$this->resultArr[self::showMessage('checkbox2')] = self::showMessage('user select') . ' ' . self::showMessage('checkbox2');
			}
			if(!empty($this->selectedCheckboxes['checkbox3'])) {
				$this->result .= self::showMessage('user select') . ' ' . self::showMessage('checkbox3') . "\n";
				$this->resultArr[self::showMessage('checkbox3')] = self::showMessage('user select') . ' ' . self::showMessage('checkbox3');
			}
			if($this->selectedRadiobutton == 'radiobutton1') {
				$this->result .= self::showMessage('user select') . ' ' . self::showMessage('radiobutton1') . "\n";
				$this->resultArr[self::showMessage('radiobutton1')] = self::showMessage('user select') . ' ' . self::showMessage('radiobutton1');
			}
			if($this->selectedRadiobutton == 'radiobutton2') {
				$this->result .= self::showMessage('user select') . ' ' . self::showMessage('radiobutton2') . "\n";
				$this->resultArr[self::showMessage('radiobutton2')] = self::showMessage('user select') . ' ' . self::showMessage('radiobutton2');
			}
			if($this->selectedRadiobutton == 'radiobutton3') {
				$this->result .= self::showMessage('user select') . ' ' . self::showMessage('radiobutton3') . "\n";
				$this->resultArr[self::showMessage('radiobutton3')] = self::showMessage('user select') . ' ' . self::showMessage('radiobutton3');
			}
			if($this->selectedOption == 'option1') {
				$this->result .= self::showMessage('user select') . ' ' . self::showMessage('option1') . "\n";
				$this->resultArr[self::showMessage('option1')] = self::showMessage('user select') . ' ' . self::showMessage('option1');
			}
			if($this->selectedOption == 'option2') {
				$this->result .= self::showMessage('user select') . ' ' . self::showMessage('option2') . "\n";
				$this->resultArr[self::showMessage('option2')] = self::showMessage('user select') . ' ' . self::showMessage('option2');
			}
			if($this->selectedOption == 'option3') {
				$this->result .= self::showMessage('user select') . ' ' . self::showMessage('option3') . "\n";
				$this->resultArr[self::showMessage('option3')] = self::showMessage('user select') . ' ' . self::showMessage('option3');
			}
		}
		
		public function saveCacheFiles() {
			mb_internal_encoding('UTF-8');
			mb_regex_encoding('UTF-8');
			
			$dateCode = date('Y-m-d_H-i-s', time());
			$randCode = rand(0, 1000);
			$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
			
			$root = $_SERVER['HTTP_HOST'];
			if($root == 'corpus.by') $root = 'https://corpus.by';
			$serviceName = 'ServiceDemonstrator';
			$sendersName = 'Service Demonstrator';
			$recipient = 'corpus.by@gmail.com';
			$subject = "$sendersName from IP $ip";
			$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
			$mailBody .= "$root/$serviceName/ дасылае інфармацыю аб актыўнасці карыстальніка з IP $ip." . self::BR . self::BR;
			$textLength = mb_strlen($this->text);
			$pages = round($textLength/2300, 1);
			
			$cachePath = dirname(dirname(__FILE__)) . "/_cache";
			if(!file_exists($cachePath)) mkdir($cachePath);
			$cachePath = "$cachePath/$serviceName";
			if(!file_exists($cachePath)) mkdir($cachePath);
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_in.txt';
			$path = "$cachePath/in/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			$cacheText = preg_replace("/(^\s+)|(\s+$)/us", '', $this->text);
			fwrite($newFile, $cacheText);
			fclose($newFile);
			$url = $root . "/showCache.php?s=$serviceName&t=in&f=$filename";
			if(mb_strlen($cacheText)) {
				$mailBody .= "Тэкст ($textLength сімв., прыкладна $pages ст. па 2300 сімв. на старонку) пачынаецца з:" . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cacheText, $matches);
				$str = str_replace("\n", self::BR, trim($matches[0]));
				if(mb_strlen($str) < 300) {
					$mailBody .= '<blockquote><i>' . $str . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				}
				else {
					$mailBody .= '<blockquote><i>' . mb_substr($str, 0, 300) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				}
			}
			$mailBody .= self::BR;
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_out.txt';
			$path = "$cachePath/out/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			$cacheText = preg_replace("/(^\s+)|(\s+$)/us", "", $this->result);
			fwrite($newFile, $cacheText);
			fclose($newFile);
			$url = $root . "/showCache.php?s=$serviceName&t=out&f=$filename";
			if(mb_strlen($cacheText)) {
				$mailBody .= "Вынік пачынаецца з:" . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cacheText, $matches);
				$mailBody .= '<blockquote><i>' . str_replace("\n", self::BR, trim($matches[0])) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
			}
			$mailBody .= self::BR;
			
			$header  = "MIME-Version: 1.0\r\n";
			$header .= "Content-type: text/html; charset=utf-8\r\n";
			$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
			mail($recipient, $subject, $mailBody, $header);
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_e.txt';
			$path = "$cachePath/email/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			fwrite($newFile, join("\n", array("recipient: \t" . $recipient, "subject: \t" . $subject, "\n\tMAIL BODY\n\n" . $mailBody, $header)));
			fclose($newFile);
		}
		
		public function getResult() {
			return $this->result;
		}
		
		public function getResultArr() {
			return $this->resultArr;
		}
	}
?>