<?php
	header("Content-Type: text/html; charset=utf-8");
	$ini = parse_ini_file('service.ini');
	include_once 'ServiceDemonstrator.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	$languages = ServiceDemonstrator::loadLanguages();
	ServiceDemonstrator::loadLocalization($lang);
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
	<head>
		<title><?php echo ServiceDemonstrator::showMessage('title'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href='css/theme.css'>
		<link rel="icon" type="image/x-icon" href="img/favicon.ico">
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
		<?php include_once 'analyticstracking.php'; ?>
		<script>
			var inputTextDefault = "<?php echo str_replace("\"", '\"', str_replace("\n", '\\n', ServiceDemonstrator::showMessage('default input'))); ?>";
			$(document).ready(function () {
				$(document).on('click', 'button#MainButtonId', function() {
					$('#resultBlockId').show('slow');
					$('#resultId').empty();
					$('#resultId').prepend($('<img>', { src: "img/loading.gif"}));
					var checkbox1 = $('input:checkbox[name=checkbox1]').prop('checked') == true ? 1 : 0;
					var checkbox2 = $('input:checkbox[name=checkbox2]').prop('checked') == true ? 1 : 0;
					var checkbox3 = $('input:checkbox[name=checkbox3]').prop('checked') == true ? 1 : 0;
					$.ajax({
						type: 'POST',
						url: 'https://corpus.by/ServiceDemonstrator/api.php',
						data: {
							'localization': '<?php echo $lang; ?>',
							'text': $('textarea#inputTextId').val(),
							'checkbox1': checkbox1,
							'checkbox2': checkbox2,
							'checkbox3': checkbox3,
							'mode': $("input:radio[name=mode]:checked").val(),
							'selector': $('select#selectorId').val()
						},
						success: function(msg) {
							var result = jQuery.parseJSON(msg);
							var output = document.createElement("textarea");
							output.setAttribute("id", "resultId");
							output.setAttribute("class", "form-control");
							output.setAttribute("rows", "10");
							output.setAttribute("placeholder", "Recognized text");
							output.setAttribute("readonly", "");
							var outputInnerText = document.createTextNode(
								"<?php echo ServiceDemonstrator::showMessage('entered text'); ?>\n"
								+ result.text + "\n\n<?php echo ServiceDemonstrator::showMessage('selected settings'); ?>\n"
								+ Object.keys(result.resultArr).reduce(
									function(previousValue, currentValue) {
										return(previousValue + "* " + currentValue + " — " + result.resultArr[currentValue] + "\n");
									},
									"",
								)
							);
							output.append(outputInnerText);
							$('#resultId').html(output);
						},
						error: function() {
							$('#resultId').html('ERROR');
						}
					});
				});
			});
		</script>
	</head>
	<body>
		<!-- Novigation -->
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/"><img style="max-width:150px; margin-top: -7px;" src="img/main-logo.png" alt=""></a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="service-name"><a href="/ServiceDemonstrator/?lang=<?php echo $lang; ?>"><?php echo ServiceDemonstrator::showMessage('title'); ?></a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="service-name"><a href="<?php echo ServiceDemonstrator::showMessage('help'); ?>" target="_blank">?</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo ServiceDemonstrator::showMessage($lang); ?><span class="caret"></span></a>
							<ul class="dropdown-menu">
								<?php
									$languages = ServiceDemonstrator::loadLanguages();
									foreach($languages as $language) {
										echo "<li><a href='?lang=$language'>" . ServiceDemonstrator::showMessage($language) . "</a></li>";
									}
								?>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- End of Novigation -->
		<div class="container theme-showcase" role="main">
			<div class="row">
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="btn-group pull-right">
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value=inputTextDefault;"><?php echo ServiceDemonstrator::showMessage('refresh'); ?></button>
									<button type="button" class="btn btn-default btn-xs" onclick="document.getElementById('inputTextId').value='';"><?php echo ServiceDemonstrator::showMessage('clear'); ?></button>
								</div>
								<h3 class="panel-title"><?php echo ServiceDemonstrator::showMessage('input'); ?></h3>
							</div>
							<div class="panel-body">
								<p><textarea class="form-control" rows="10" id = "inputTextId" name="inputText" placeholder="Enter text"><?php echo str_replace('\n', "\n", ServiceDemonstrator::showMessage('default input')); ?></textarea></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<!-- checkboxes -->
					<div class="col-md-4">
						<?php echo ServiceDemonstrator::showMessage('checkboxes'); ?><br /><br />
						<input type="checkbox" id="checkbox1" name="checkbox1" value="1" checked>
						<label for="checkbox1">
							<span class="bold"><?php echo ServiceDemonstrator::showMessage('checkbox1'); ?></span>
							<span class="small"><?php echo ServiceDemonstrator::showMessage('checkbox1 comment'); ?></span>
						</label><br />
						<input type="checkbox" id="checkbox2" name="checkbox2" value="2">
						<label for="checkbox2">
							<span class="bold"><?php echo ServiceDemonstrator::showMessage('checkbox2'); ?></span>
							<span class="small"><?php echo ServiceDemonstrator::showMessage('checkbox2 comment'); ?></span>
						</label><br />
						<input type="checkbox" id="checkbox3" name="checkbox3" value="3">
						<label for="checkbox3">
							<span class="bold"><?php echo ServiceDemonstrator::showMessage('checkbox3'); ?></span>
							<span class="small"><?php echo ServiceDemonstrator::showMessage('checkbox3 comment'); ?></span>
						</label><br />
					</div>
					<!-- radiobuttons -->
					<div class="col-md-4">
						<?php echo ServiceDemonstrator::showMessage('radiobuttons'); ?><br /><br />
						<input type="radio" name="mode" value="radiobutton1" id="radiobutton1" checked>
						<label for="radiobutton1">
							<span class="bold"><?php echo ServiceDemonstrator::showMessage('radiobutton1'); ?></span>
							<span class="small"><?php echo ServiceDemonstrator::showMessage('radiobutton1 comment'); ?></span>
						</label><br />
						<input type="radio" name="mode" value="radiobutton2" id="radiobutton2">
						<label for="radiobutton2">
							<span class="bold"><?php echo ServiceDemonstrator::showMessage('radiobutton2'); ?></span>
							<span class="small"><?php echo ServiceDemonstrator::showMessage('radiobutton2 comment'); ?></span>
						</label><br />
						<input type="radio" name="mode" value="radiobutton3" id="radiobutton3">
						<label for="radiobutton3">
							<span class="bold"><?php echo ServiceDemonstrator::showMessage('radiobutton3'); ?></span>
							<span class="small"><?php echo ServiceDemonstrator::showMessage('radiobutton3 comment'); ?></span>
						</label><br />
					</div>
					<!-- options -->
					<div class="col-md-4">
						<?php echo ServiceDemonstrator::showMessage('options'); ?><br /><br />
						<select id="selectorId" name="selector" class="selector-primary">
							<option value="option1"><?php echo ServiceDemonstrator::showMessage('option1'); ?></option>
							<option value="option2"><?php echo ServiceDemonstrator::showMessage('option2'); ?></option>
							<option value="option3"><?php echo ServiceDemonstrator::showMessage('option3'); ?></option>
						</select>
					</div>
				</div>
				<div class="col-md-12">
					<button type="button" id="MainButtonId" name="MainButton" class="button-primary"><?php echo ServiceDemonstrator::showMessage('button'); ?></button>
				</div>
				<div class="col-md-12" id="resultBlockId" style="display: none;">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title"><?php echo ServiceDemonstrator::showMessage('result'); ?></h3>
							</div>
							<div class="panel-body">
								<p id="resultId"></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-body">
								<?php echo ServiceDemonstrator::showMessage('service code'); ?>&nbsp;<a href="https://gitlab.com/ssrlab1/ServiceDemonstrator" target="_blank"><?php echo ServiceDemonstrator::showMessage('reference'); ?></a>.
								<br />
								<a href="https://gitlab.com/ssrlab1/ServiceDemonstrator/-/issues/new" target="_blank"><?php echo ServiceDemonstrator::showMessage('suggestions'); ?></a>.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="footer">
			<div class="container">
				<p class="text-muted">
					<?php echo ServiceDemonstrator::showMessage('contact e-mail'); ?>
					<a href="mailto:corpus.by@gmail.com">corpus.by@gmail.com</a>.<br />
					<?php echo ServiceDemonstrator::showMessage('other prototypes'); ?>
					<a href="https://corpus.by/?lang=<?php echo $lang; ?>">corpus.by</a>,&nbsp;<a href="https://ssrlab.by">ssrlab.by</a>.
				</p>
				<p class="text-muted">
					<?php echo ServiceDemonstrator::showMessage('laboratory'), ', ', $ini['year']; if($ini['year'] !== date('Y')) echo '—', date('Y'); ?>
				</p>
			</div>
		</footer>
	</body>
</html>
<?php ServiceDemonstrator::sendErrorList($lang); ?>