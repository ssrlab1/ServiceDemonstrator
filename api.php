<?php
	header("Content-type: text/html;  charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
		
	$localization = isset($_POST['localization']) ? $_POST['localization'] : 'en';
	$text = isset($_POST['text']) ? $_POST['text'] : '';
	$checkbox1 = isset($_POST['checkbox1']) ? $_POST['checkbox1'] : 0;
	$checkbox2 = isset($_POST['checkbox2']) ? $_POST['checkbox2'] : 0;
	$checkbox3 = isset($_POST['checkbox3']) ? $_POST['checkbox3'] : 0;
	$checkboxes = array('checkbox1' => $checkbox1, 'checkbox2' => $checkbox2, 'checkbox3' => $checkbox3);
	$mode = isset($_POST['mode']) ? $_POST['mode'] : '';
	$selector = isset($_POST['selector']) ? $_POST['selector'] : '';
	
	include_once 'ServiceDemonstrator.php';
	ServiceDemonstrator::loadLocalization($localization);
	
	$msg = '';
	if(!empty($text)) {
		$ServiceDemonstrator = new ServiceDemonstrator($checkboxes, $mode, $selector);
		$ServiceDemonstrator->setText($text);
		$ServiceDemonstrator->run();
		$ServiceDemonstrator->saveCacheFiles();

		$result['text'] = $text;
		$result['result'] = $ServiceDemonstrator->getResult();
		$result['resultArr'] = $ServiceDemonstrator->getResultArr();
		$msg = json_encode($result);
	}
	echo $msg;
?>